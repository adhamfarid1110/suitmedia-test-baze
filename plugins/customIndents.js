import Vue from 'vue'

export function generateCustomIndent(iterations) {
	return '\u00A0'.repeat(iterations * 4) // Repeats the Unicode representation of &nbsp; based on the number of iterations
}
